</div>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery.mobile-1.3.2.min.js"></script>
    <script src='/vendor/ckeditor/ckeditor.js'></script>
    <script src='/vendor/ckeditor/adapters/jquery.js'></script>
    <script src='/vendor/jquery_fileupload/jquery.iframe-transport.js'></script>
    <script src='/vendor/jquery.ui.widget.js'></script>
    <script src='/vendor/jquery_fileupload/jquery.fileupload.js'></script>
    <script src='/vendor/jquery_fileupload/jquery.fileupload-process.js'></script>
    <script src='/vendor/jquery_fileupload/jquery.fileupload-validate.js'></script>
    <script src="/vendor/smartmenus/jquery.smartmenus.min.js"></script>
    <script src="/vendor/highlight/highlight.pack.js"></script>
    <script src="/js/application.js"></script>
    <script src="/js/utilities.js"></script>
    <script src="/js/media.js"></script>
    </body>
</html>
