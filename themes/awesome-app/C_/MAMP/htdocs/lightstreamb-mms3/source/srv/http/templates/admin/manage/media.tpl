{include file="header.tpl"}
{include file="admin{$smarty.const.DS}top_nav.tpl"}
<!-- Main Content -->
<!-- Header -->
<div data-role="header" data-theme="c" class="media-controls">
    <form method="POST" action="/admin/utilities/upload.php" id="upload-media-form" data-ajax="false" enctype="multipart/form-data">
        
        <div style="position: absolute; right: 50px; top: 10px; width: 30%;" align="right">
            {t}Free Disk Space: {/t} {$humanReadableDiskFreeSpace}
        </div>
        
        <div data-role="controlgroup" data-type="horizontal">
            <label data-role="button" class="file-upload" data-icon="arrow-u">{t}Upload Files{/t}
                <input class="file-upload-input" type="file" name="files[]" data-role="none" multiple>
                <input type="hidden" name="current_directory_id" id="current-directory-id-input" value="{$currentDirectoryID}">
            </label>
            <a href="#add_new_folder" data-role="button" data-icon="plus" data-rel="popup" data-position-to="window" class="add-folder-link">{t}New Folder{/t}</a>
        </div>
        
    </form>

    <div class="move-delete-controls ui-disabled" data-role="controlgroup" data-type="horizontal">
        <a href="#move_media" data-role="button" data-icon="forward" data-rel="popup" data-position-to="window" class="move-selected-files">{t}Move Selected{/t}</a>
        <a href="#delete_media" data-role="button" data-icon="delete" data-rel="popup" data-position-to="window" class="delete-selected-files" data-theme="r">{t}Delete Selected{/t}</a>
    </div>

    <div class="change-symlink-controls ui-disabled" data-role="controlgroup" data-type="horizontal">
        <a href="#reset-symlinks" data-role="button" data-icon="refresh" data-rel="popup" data-position-to="window" class="reset-media-symlinks">{t}Reset Availability{/t}</a>
        <a href="#change-symlinks" data-role="button" data-icon="check" data-rel="popup" data-position-to="window" class="change-media-symlinks">{t}Change Availability{/t}</a>
    </div>
    <form id="checks" action="/login" method="post">
    <div class="" data-role="controlgroup" data-type="horizontal">
      <label><input type="checkbox" value="all" id="all">All</label>
       <!--<input type="button" name="Check_All" value="Check All"
onClick="CheckAll(document.theBoxes.check_list)">
<input type="button" name="Un_CheckAll" value="Uncheck All"
onClick="UnCheckAll(document.theBoxes.check_list)">
       -->
       <!-- <a data-role="button" onclick="javascript:checkAll('theBoxes', true);" href="javascript:void(0);"  >{t}Select All{/t}</a>
        <a data-role="button" data-theme="r"  onclick="javascript:checkAll('theBoxes', false);" href="javascript:void(0);" >{t}Deselect All{/t}</a>-->
    </div>
    
</div>

<div data-role="header" data-theme="c">
    <div id="upload_messages" style="display: none;"></div>
    <div id="progress_bars" style=""></div>
</div>

{if $parentDirectoryId != ''}
    <div data-role="content">
        <a href="{$currentFile}" onclick="showLoadingDisableClicks()" data-role="button" data-icon="home" data-mini="true" data-inline="true" class="ui-btn-right" data-ajax="false">{t}Home{/t}</a>
        <a href="{$currentFile}?media_location_id={$parentDirectoryId}" onclick="showLoadingDisableClicks()" data-icon="back" class="ui-btn-left" data-role="button" data-mini="true" data-ajax="false">{t}Back{/t}</a>
    </div>
{/if}
<div data-role="content">
    <ul data-role="listview" class="has-left-radio">
        {foreach from=$items item=mediaItem}
            <li class="media-listview-element admin-media-listview-element" data-icon="false">
                <div class="left-radio admin-left-radio">
                    <label for="media_select">
                        <input type="checkbox" name="media_select" class="media_select_checkbox" data-media-id="{$mediaItem['inode']}"/>
                    </label>
                </div>
                {if $mediaItem['mime_type'] eq 'directory'}
                    <a href="{$currentFile}?media_location_id={$mediaItem['inode']}" data-ajax="false" onclick="showLoadingDisableClicks()">
                        <div>
                            <img src="/images/file_icons/folder.png">
                            <p class="admin-directory-title">{$mediaItem['file_name']}</p>
                        </div>
                    </a>
                {else}
                    <a href="/download.php?id={$mediaItem['inode']}&f=false" target="_blank">
                        <div>
                            {if file_exists("{$smarty.const.BASE_DIR}/htdocs/images/file_icons/{$mediaItem['file_extension']}.png")} 
                                <img src="/images/file_icons/{$mediaItem['file_extension']}.png"/>
                            {else}
                                <img src="/images/file_icons/_blank.png"/>
                            {/if}
                            <p class="admin-media-title">{$mediaItem['file_name']}</p>
                        </div>
                    </a>
                {/if}
                <div data-role="controlgroup" data-type="horizontal" class="media-symlink-controls">
                    <label for="bluetooth" {if $mediaItem['bluetooth_status'] === 1}class="ui-checkbox-indeterminate"{/if}>
                        <input type="checkbox" name="bluetooth" class="media-symlink-checkbox" data-mini="true" data-media-id="{$mediaItem['inode']}"
                               {if $mediaItem['bluetooth_status'] === 2}checked{/if} />Bluetooth</label>
                    <label for="usb" {if $mediaItem['usb_status'] === 1}class="ui-checkbox-indeterminate"{/if}>
                        <input type="checkbox" name="usb" class="media-symlink-checkbox" data-mini="true" data-media-id="{$mediaItem['inode']}"
                               {if $mediaItem['usb_status'] === 2}checked{/if} />USB</label>
                    <label for="wifi" {if $mediaItem['wifi_status'] === 1}class="ui-checkbox-indeterminate"{/if}>
                        <input type="checkbox" name="wifi" class="media-symlink-checkbox" data-mini="true" data-media-id="{$mediaItem['inode']}"
                               {if $mediaItem['wifi_status'] === 2}checked{/if} />WiFi</label>
                </div>
                <div class="split-custom-wrapper admin-split-custom-wrapper">
                    <a href="#manage_media" data-role="button" class="split-custom-button manage-media-link" data-icon="info" data-rel="popup" data-iconpos="notext" data-media-id="{$mediaItem['inode']}" data-media-name="{$mediaItem['file_name']}" data-theme="a" data-position-to="window">{t}Edit{/t}</a>
                </div>
            </li>
        {/foreach}
    </ul>
</div>
    </form>
{if $form_errors}
    <script type="text/javascript">
        setTimeout(function () {
            showFormerrorsPopup();
        }, 1000);
    </script>
{/if}

{include file="formerrors_popup.tpl"}
{include file="admin{$smarty.const.DS}manage{$smarty.const.DS}manage_media_popups.tpl"}
{include file="footer.tpl"}
