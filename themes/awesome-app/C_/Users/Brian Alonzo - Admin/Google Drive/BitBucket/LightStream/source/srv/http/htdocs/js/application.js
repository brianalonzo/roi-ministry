/**
 * NOTE: Any pages depending on an onclick method, should have links leading to it set as data-ajax="false", otherwise they will get fired multiple times
 */
/**
 * An array of all the files uploaded states.  If one is true, then it redirects 
 *
 */
var uploadedFilesStates;
/**
 * Stores the interval for updating the log view
 *
 * @param Object
 */
var logUpdaterInterval = null;
/**
 * Stores the interval for updating zip pid status
 *
 * @param Object
 */
var zipPidUpdaterInterval = null;
/**
 * Total number of seconds to run the updater
 *
 * @param integer
 */
var logUpdaterSeconds = 15000;
/**
 * Handle on load functionality
 *
 */
$(function () {
    setupAdminNav();
    /**
     * If the page is loaded by AJAX
     */
    $(document).bind('pagebeforeshow', function (event, ui) {
        var currentPageID = $.mobile.activePage.attr('id');
        if (currentPageID == 'manage-user-interface') {
            showCKEditor();
        };
        if (currentPageID == 'manage-media') {
            setupManageMediaPage();
        };
        if (currentPageID == 'settings-update') {
            setupSettingsUpdateFrmwrPage();
        };
        if (currentPageID == 'settings-reboot') {
            setupRebootPage();
        };
        if (currentPageID == 'display-logs') {
            setupDisplayLogsPage();
        } else {
            clearInterval(logUpdaterInterval);
        };
        if (pageId == 'download_dir_wait') {
            setupZipPidChecker();
        } else {
            clearInterval(zipPidUpdaterInterval);
            zipPidUpdaterInterval = null;
        };
        if (currentPageID == 'stats-blacklist-page') {
            setupCollapsableWithCheckboxes('input#devices_to_clear');
        };
        if (currentPageID == 'settings-wifi') {
            watchAndWarnInputChange($('#wifi_channel_input'), $('#settings-wifi-form'), localizedMessages['change_wifi_channel_warning']);
        };
        if (currentPageID == 'dashboard-page'){
          setupDashboardPage();
        };
        setupAdminNav();
    });
    /**
     * If the page is being removed from DOM
     *
     */
    $(document).bind('pageremove', function (event, ui) {
        var removingPageID = $(event.target).attr('id');
        if (removingPageID == 'manage-user-interface') {
        };
    });
    /**
     * If the page is loaded by users browser
     *
     */
    var pages = $('*[data-role="page"]').length;
    if (pages == 1) {
        var pageId = $('*[data-role="page"]').eq(0).attr('id');
        if (pageId == 'manage-user-interface') {
            showCKEditor();
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'stats-blacklist-page') {
            setupCollapsableWithCheckboxes('input#devices_to_clear');
        };
        if ((pageId == 'stats-wifi-page') || (pageId == 'stats-bluetooth-page') || (pageId == 'stats-sd-copy-page') || (pageId == 'stats-blacklist-page')) {
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'settings-language') {
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'settings-update') {
            setupSettingsUpdateFrmwrPage();
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                $("#success_popup").popup('open');
                //wait for some time, then try to redirect to this page
                var redirectTo = window.location.href.split("?")[0];
                setTimeout(function () {
                    window.location.href = redirectTo;
                }, 600000);
            };
            if (saved == 'false') {
                $("#error_popup").popup('open');
            };
        };
        if (pageId == 'settings-account') {
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'settings-wifi') {
            watchAndWarnInputChange($('#wifi_channel_input'), $('#settings-wifi-form'), localizedMessages['change_wifi_channel_warning']);
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'settings-bluetooth') {
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'settings-reboot') {
            setupRebootPage();
        };
        if (pageId == 'display-logs') {
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                // openDialog was removed CV 2015-07-16
            };
            setupDisplayLogsPage();
        } else {
            clearInterval(logUpdaterInterval);
            logUpdaterInterval = null;
        };

        if (pageId == 'download_dir_wait') {
            setupZipPidChecker();
        } else {
            clearInterval(zipPidUpdaterInterval);
            zipPidUpdaterInterval = null;
        };
        if (pageId == 'manage-media') {
            setupManageMediaPage();
            var saved = getParameterByName('saved_successfully', window.location.href);
            if (saved == 'true') {
                var method = getParameterByName('method', window.location.href);
                switch (method) {
                    case 'delete':
                        var message = localizedMessages['media_deleted'];
                        break;
                    case 'put_rename':
                        var message = localizedMessages['media_renamed'];
                        break;
                    case 'put_move':
                        var message = localizedMessages['media_moved'];
                        break;
                    case 'post':
                        var message = localizedMessages['folder_created'];
                        break;
                    case 'uploads':
                        var message = localizedMessages['files_uploaded'];
                        break;
                    default:
                        var message = localizedMessages['media_updated'];
                };
                // openDialog was removed CV 2015-07-16
            };
        };
        if (pageId == 'dashboard-page'){
          setupDashboardPage();
        };
    };
});
function watchAndWarnInputChange(formEle, appendToEle, warning) {
    formEle.focus(function (event) {
        if ($('div#watch-and-warn-message').length == 0) {
            var div = $('<div/>').attr('id', 'watch-and-warn-message').addClass('alert alert-error').text(warning);
            $(appendToEle).prepend(div);
        };
    });
};
/**
 * initialize Javascript for the Dashboard page
 * @return void
 */
function setupDashboardPage(){
    $("a.delete-zip-dir-link").click(function (event) {
        $("#delete_zip_dir_popup").popup('open');
    });
};
/**
 * initialize Javascript for the Checkboxes in a collapsable element
 * Values will be stored comma seperated in the selected element
 * @link http://jsfiddle.net/ezanker/UwkWx/124/
 * @param String passValueEle The JQuery selector for where to pass the checkbox value when checked.
 * @return void
 */
function setupCollapsableWithCheckboxes(passValueEle) {
    $('.checkbox-collapse-label .ui-icon').click(function (e) {
        e.stopPropagation();
        var newValue = '';
        var value = $(this).closest('label').siblings('input').eq(0).val();
        var passValueElement = $(passValueEle);
        var currentValue = passValueElement.val();
        if ($(this).hasClass('ui-icon-checkbox-on')) {
            $(this).closest('label').attr("data-icon", "checkbox-off");
            $(this).addClass('ui-icon-checkbox-off');
            $(this).removeClass('ui-icon-checkbox-on');
            $(this).closest('label').addClass('ui-checkbox-off');
            $(this).closest('label').removeClass('ui-checkbox-on');
            /**
             * Remove the value from the passValueElement
             */
            var tmp = currentValue.split(',');
            var index = tmp.indexOf(value);
            if (index !== -1) {
                tmp.splice(index, 1);
                newValue = tmp.join(',');
            }
        } else {
            $(this).closest('label').attr("data-icon", "checkbox-on");
            $(this).addClass('ui-icon-checkbox-on');
            $(this).removeClass('ui-icon-checkbox-off');
            $(this).closest('label').addClass('ui-checkbox-on');
            $(this).closest('label').removeClass('ui-checkbox-off');
            /**
             * Add the value to the passValueElement
             */
            newValue = currentValue ? (currentValue + "," + value) : value;
        }
        passValueElement.val(newValue);
        return false;
    });
};
/**
 * initialize Javascript for the Logs Display page
 * @return void
 */
function setupDisplayLogsPage() {
    hljs.initHighlightingOnLoad();
    if (logUpdaterInterval != null) {
        clearInterval(logUpdaterInterval);
    }
    logUpdaterInterval = setInterval(
            function () {
                updateLogContent();
            },
            logUpdaterSeconds
            );
    $('a#clear-log-trigger').click(function (event) {
        $('form#clear-log-form').submit();
        return false;
    });
};
/**
 * retrieve the most current log data, and prepend to the view
 * @return void
 */
function updateLogContent() {
    var log = getParameterByName('log', window.location.href);
    var url = "/admin/logs/display.php?log=" + log + "&ajax=true";
    $.mobile.loading('show');
    $.get(url, function (data) {
        if (data != "") {
            $('pre#log-data code').prepend(data + "\n");
        };
        $.mobile.loading('hide');
    });
};
function setupZipPidChecker() {
    $.mobile.loading('show');
    if (zipPidUpdaterInterval != null) {
        clearInterval(zipPidUpdaterInterval);
        zipPidUpdaterInterval = null;
    };
    var should_check_pid = $('div.should_check_pid-div').attr('id');
    if (should_check_pid != 'false') {
        zipPidUpdaterInterval = setInterval(
                function () {
                    checkZipPidStatus();
                },
                5000
                );
    };
};
function checkZipPidStatus() {
    var pid = $('div.pid-div').attr('id');
    var url = "/get_pid_status.php?pid=" + pid;
    $.get(url, function (data) {
        // check fi the pid has finished running
        if (data != "1") {

            // stop the pid updater interval
            if (zipPidUpdaterInterval != null) {
                clearInterval(zipPidUpdaterInterval);
                zipPidUpdaterInterval = null;
            };

            // hide the loading bar
            $.mobile.loading('hide');

            // start the download
            window.location.href = window.location.href;

        };
    });
};
/**
 * initialize Javascript for the Manage Media page
 * @return void
 */
function setupManageMediaPage() {
    uploadedFilesStates = new Array();
	
	// Iterate through input elements with class 'media-symlink-checkbox'.
	$('input.media-symlink-checkbox').each(function(index, el) {
		// Define selector for label element associated with current input element.
		var associatedLabelElement = $($(this).siblings('label').get(0));
		// Check whether associated label has class 'ui-checkbox-indeterminate'.
		if (associatedLabelElement.hasClass('ui-checkbox-indeterminate')) {
			// Set indeterminate property of input element to true.
			$(this).prop('indeterminate', true);
		};
	});
	
	// Define function to call when any media symlink checkbox is clicked.
	$('input.media-symlink-checkbox').click(function(event) {
		// Check whether control group div.change-symlink-controls is disabled.
		if ($('div.change-symlink-controls').hasClass('ui-disabled')) {
			// Enable control group div.change-symlink-controls.
			$('div.change-symlink-controls').removeClass('ui-disabled');
		}
		// Define selector for label element associated with current input element.
		var associatedLabelElement = $($(this).siblings('label').get(0));
		// Check whether associated label has class 'ui-checkbox-indeterminate'.
		if (associatedLabelElement.hasClass('ui-checkbox-indeterminate')) {
			// Change indeterminate property of input element to false.
			$(this).prop('indeterminate', false);
			// Remove class 'ui-checkbox-indeterminate' from associated label.
			associatedLabelElement.removeClass('ui-checkbox-indeterminate');
		};
	});
	
	// Define what happens when the chane-media-symlinks button is clicked.
	$("a.change-media-symlinks").click(function(event) {
		// Define list of media IDs corresponding to checked symlink boxes.
		var checkedSymlinkMediaIds = getCheckedSymlinkMediaIds();
		// Define list of media IDs corresponding to unchecked symlink boxes.
		var uncheckedSymlinkMediaIds = getUncheckedSymlinkMediaIds();
		
		// Define list of symlink types corresponding to checked symlink boxes.
		var checkedSymlinkDirectories = getCheckedSymlinkDirectories();
		// Define list of symlink types corresponding to unchecked symlink boxes.
		var uncheckedSymlinkDirectories = getUncheckedSymlinkDirectories();

		// Set value of input attribute change-symlinks-form-checked-media-ids to value of checkedSymlinkMediaIds.
		$('input#change-symlinks-form-checked-media-ids').val(checkedSymlinkMediaIds);
		// Set value of input attribute change-symlinks-form-unchecked-media-ids to value of uncheckedSymlinkMediaIds.
		$('input#change-symlinks-form-unchecked-media-ids').val(uncheckedSymlinkMediaIds);
		
		// Set value of input attribute change-symlinks-form-checked-symlink-directories to value of checkedSymlinkDirectories.
		$('input#change-symlinks-form-checked-symlink-directories').val(checkedSymlinkDirectories);
		// Set value of input attribute change-symlinks-form-unchecked-symlink-directories to value of uncheckedSymlinkDirectories.
		$('input#change-symlinks-form-unchecked-symlink-directories').val(uncheckedSymlinkDirectories);
		
		// Open change-symlinks popup.
		$("#change-symlinks").popup('open');
	});
	
    $('input.media_select_checkbox').change(function (event) {
        var checked = getMediaIds();
        if (checked.length == 0) {
            $('div.move-delete-controls').addClass('ui-disabled');
        } else {
            $('div.move-delete-controls').removeClass('ui-disabled');
        };
    });
	
    $("a.manage-media-link").click(function (event) {
        var currentMediaID = $(this).attr('data-media-id');
        var currentMediaName = $(this).attr('data-media-name');
        $('input#edit-form-media-title').val(currentMediaName);
        $('input#edit-form-media-id').val(currentMediaID);
        $("#manage_media").popup('open');
    });
    $("a.delete-media-link").click(function (event) {
        var currentMediaID = $(this).attr('data-media-id');
        var currentMediaName = $(this).attr('data-media-name');
        $('span#delete-media-name').text(currentMediaName);
        $('input#delete-form-media-name').val(currentMediaName);
        $('input#delete-form-media-id').val(currentMediaID);
        $("#delete_media").popup('open');
    });
    $("a.delete-selected-files").click(function (event) {
        var currentMediaIDs = getMediaIds();
        $('input#delete-form-media-id').val(currentMediaIDs);
        $('span#delete-media-name').text("All Selected Files");
        $("#delete_media").popup('open');
    });
    $("a.move-selected-files").click(function (event) {
        var currentMediaIDs = getMediaIds();
        $('input#move-form-media-id').val(currentMediaIDs);
        $("#move_media").popup('open');
    });
    $("a.add-folder-link").click(function (event) {
        $("#add_new_folder").popup('open');
    });
    $('.file-upload-input').fileupload({
        dataType: 'json',
        url: '/admin/utilities/upload.php',
        maxFileSize: maxUploadSize,
        submit: function (e, data) {
            if ($('div#file_upload_progress').length == 0) {
                if (!$('div#progress_bars').is(':visible')) {
                    $('div#progress_bars').show('slow');
                };
                createProgressBar('div#progress_bars', localizedMessages['uploading_files'], 'file_upload_progress');
            };
            return true;
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            progressBar.setValue('#file_upload_progress input.upload-progress', progress);

            if (progress < 100) {
                var string1 = localizedMessages['uploading_files'];
                var string2 = createFileUploadProgressStringFromFileUploadProgressData(data);
                string1 = string1.concat(" ");
                string1 = string1.concat(string2);
                progressBar.setLabel('#file_upload_progress label', string1);
            } else {
                var string1 = localizedMessages['please_wait'];
                progressBar.setLabel('#file_upload_progress label', string1);
            };
        },
        done: function (e, data) {
            if (data.result.success == 'false') {
                appendErrorForUpload(data.result.file, data.result.message);
                uploadedFilesStates.push(false);
                $('div#file_upload_progress').hide('fast', function () {
                    $(this).html('');
                });
            } else if (data.result.success == 'true') {
                /**
                 * successful upload
                 */
                uploadedFilesStates.push(true);
            };
        },
        fail: function (e, data) {
            appendErrorForUpload("", localizedMessages['upload_failed']);
            uploadedFilesStates.push(false);
            $('div#file_upload_progress').hide('fast', function () {
                $(this).html('');
            });
        },
        stop: function () {
            if (jQuery.inArray(true, uploadedFilesStates) !== -1) {
                /**
                 * Atleast one file uploaded successfully
                 */
                var redirectTo = window.location.href.split("?")[0];
                var mediaLocationID = getParameterByName('media_location_id', window.location.href);
                var mediaType = getParameterByName('media_type', window.location.href);
                if (mediaLocationID != '') {
                    redirectTo = redirectTo + "?media_location_id=" + mediaLocationID + "&saved_successfully=true&method=uploads&media_type=" + mediaType;
                } else {
                    redirectTo = redirectTo + "?saved_successfully=true&method=uploads&media_type=" + mediaType;
                };
                $('div#file_upload_progress').fadeOut('slow', function () {
                    appendSuccessfulUpload();
                    setTimeout(function () {
                        window.location.href = redirectTo;
                    }, 2000);
                });
            };
        }
    }).bind('fileuploadprocessfail', function (e, data) {
        $.each(data.files, function (index, file) {
            uploadedFilesStates.push(false);
            appendErrorForUpload(file.name, file.error);
        });
    });
    var mediaLocationID = getParameterByName('media_location_id', window.location.href);
    $('#current-directory-id-input').val(mediaLocationID);
};

function setupSettingsUpdateFrmwrPage() {

    uploadedFilesStates = new Array();

    $('.file-upload-input').fileupload({
        dataType: 'json',
        url: '/admin/utilities/upload_firmware.php',
        maxFileSize: maxUploadSize,
        submit: function (e, data) {
            if ($('div#file_upload_progress').length == 0) {
                if (!$('div#progress_bars').is(':visible')) {
                    $('div#progress_bars').show('slow');
                };
                hideFirmwareInputSection();
                createProgressBar('div#progress_bars', localizedMessages['uploading_files'], 'file_upload_progress');
            };
            return true;
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            progressBar.setValue('#file_upload_progress input.upload-progress', progress);

            if (progress < 100) {
                var string1 = localizedMessages['uploading_file'];
                var string2 = createFileUploadProgressStringFromFileUploadProgressData(data);
                string1 = string1.concat(" ");
                string1 = string1.concat(string2);
                progressBar.setLabel('#file_upload_progress label', string1);
            } else {
                var string1 = localizedMessages['please_wait'];
                progressBar.setLabel('#file_upload_progress label', string1);
            };
        },
        done: function (e, data) {
            var string1 = "Done";
            progressBar.setLabel('#file_upload_progress label', string1);

            if (data.result.success == 'true') {
                uploadedFilesStates.push(true);
            } else {
                uploadedFilesStates.push(false);
                appendErrorForUpload(data.result.file, data.result.message);
            };
        },
        fail: function (e, data) {
            var string1 = "Fail";
            progressBar.setLabel('#file_upload_progress label', string1);
            uploadedFilesStates.push(false);
        },
        stop: function () {
            var string1 = "Stop";
            progressBar.setLabel('#file_upload_progress label', string1);

            if (jQuery.inArray(true, uploadedFilesStates) !== -1) {
                appendSuccessfulUploadWithMsg(localizedMessages['upload_frmwr_success']);
                showUpdateButton();
            } else {
                appendErrorForUpload("", localizedMessages['upload_failed']);
                var redirectTo = window.location.href.split("?")[0];
                $('div#file_upload_progress').fadeOut('slow', function () {
                });
                setTimeout(function () {
                    $('div#upload_messages').fadeOut('slow', function () {
                        var redirectTo = window.location.href.split("?")[0];
                        window.location.href = redirectTo;
                    });
                }, 4000);
            };

            $('div#file_upload_progress').hide('fast', function () {
                $(this).html('');
            });
        }
    }).bind('fileuploadprocessfail', function (e, data) {
        $.each(data.files, function (index, file) {
            uploadedFilesStates.push(false);
            appendErrorForUpload(file.name, file.error);

        });
    });
};

function createFileUploadProgressStringFromFileUploadProgressData(data) {
    var string1 = "";
    var loadedHumanReadable = bytesToHumanReadableSize(data.loaded);
    var totalHumanReadable = bytesToHumanReadableSize(data.total);
    var bitrateHumanReadable = bytesToHumanReadableSize((data.bitrate / 8));
    string1 = string1.concat(" (");
    string1 = string1.concat(loadedHumanReadable);
    string1 = string1.concat(" / ");
    string1 = string1.concat(totalHumanReadable);
    string1 = string1.concat(")");
    string1 = string1.concat(" @ ");
    string1 = string1.concat(bitrateHumanReadable);
    string1 = string1.concat("/s");
    return string1;
};

/**
 * initialize Javascript for the Reboot Settings page
 * @return void
 */
function setupRebootPage() {
    var saved = getParameterByName('saved_successfully', window.location.href);
    if (saved == 'true') {
        setTimeout(function () {
            $("#system_reboot").popup('open');
        }, 1000);
    };
    $('a.reboot').click(function (event) {
        var formToSubmit = $(this).attr('data-form');
        $('#' + formToSubmit).submit();
        return false;
    });
};
/**
 * initialize Javascript for the Admin Nav
 * @return void
 */
function setupAdminNav() {
    $('.sm').smartmenus();
    $('.menu-button').click(function () {
        var $this = $(this),
                $menu = $('.sm');
        if (!$this.hasClass('collapsed')) {
            $menu.addClass('mobile-collapsed');
            $this.addClass('collapsed');
        } else {
            $menu.removeClass('mobile-collapsed');
            $this.removeClass('collapsed');
        }
        return false;
    });
};

/**
function selectAllMediaIds() {
    console.log('selectAllMediaIds');
    $('input.media_select_checkbox').each(function (index, el) {
        console.log('selectAllMediaIds');
        this.checked = true;
    });
}
;

function deselectAllMediaIds() {
    console.log('deselectAllMediaIds');  
    $('input.media_select_checkbox').each(function (index, el)  {
        this.prop('checked', false);
    });
}
;
**/

function checkAll(theBoxes, checktoggle)
{
  var checkboxes = new Array(); 
  checkboxes = document[theBoxes].getElementsByTagName('input');
 
  for (var i=0; i<checkboxes.length; i++)  {
    if (checkboxes[i].type == 'checkbox')   {
      checkboxes[i].checked = checktoggle;
    }
  }
}

/**
 * Retreive the selected media ids, and return as a comma seperated string
 * @return String the comma seperated ids
 */
function getMediaIds() {
    var idArray = Array();
    $('input.media_select_checkbox').each(function (index, el) {
        if (this.checked) {
            idArray.push($(this).attr('data-media-id'));
        };
    });
    return idArray.join(',');
};

/**
 * Retreive checked symlink media IDs and return as comma-seperated string.
 * @return String Comma-seperated checked symlink media IDs
 */
function getCheckedSymlinkMediaIds() {
	// Define empty array for storing media IDs that correspond to checked symlink boxes.
	var checkedSymlinkMediaIdArray = Array();

	// Iterate through input elements of class 'media-symlink-checkbox'.
	$('input.media-symlink-checkbox').each(function(index, el) {
		// Determine whether element is checked.
		if ($(this).prop('checked')) {
			// Add value of input attribute 'data-media-id' to checkedSymlinkMediaIdArray.
			checkedSymlinkMediaIdArray.push($(this).attr('data-media-id'));
		};
	});
	
	// Return string representation of checkedSymlinkMediaIdArray (elements separated by commas).
	return checkedSymlinkMediaIdArray.join(',');
};

/**
 * Retreive unchecked symlink media IDs and return as comma-seperated string.
 * @return String Comma-seperated unchecked symlink media IDs
 */
function getUncheckedSymlinkMediaIds() {
	// Define empty array for storing media IDs that correspond to unchecked symlink boxes.
	var uncheckedSymlinkMediaIdArray = Array();
	
	// Iterate through input elements of class 'media-symlink-checkbox'.
	$('input.media-symlink-checkbox').each(function(index, el) {
		// Determine whether element is unchecked.
		if (!$(this).prop('checked') && !$(this).prop('indeterminate')) {
			// Add value of input attribute 'data-media-id' to uncheckedSymlinkMediaIdArray.
			uncheckedSymlinkMediaIdArray.push($(this).attr('data-media-id'));
		};
	});
	
	// Return string representation of uncheckedSymlinkMediaIdArray (elements separated by commas).
	return uncheckedSymlinkMediaIdArray.join(',');
};

/**
 * Retreive checked symlink directories and return as comma-seperated string.
 * @return String Comma-seperated checked symlink directories
 */
function getCheckedSymlinkDirectories() {
	// Define empty array for symlink directories.
	var checkedSymlinkDirectoryArray = Array();
	
	// Iterate through input elements of class 'media-symlink-checkbox'.
	$('input.media-symlink-checkbox').each(function(index, el) {
		// Determine whether element is checked.
		if ($(this).prop('checked')) {
			// Add value of input attribute 'name' to checkedSymlinkDirectoriesArray.
			checkedSymlinkDirectoryArray.push($(this).attr('name'));
		};
	});
	
	// Return string representation of checkedSymlinkDirectoriesArray (elements separated by commas).
	return checkedSymlinkDirectoryArray.join(',');
};

/**
 * Retreive checked symlink directories and return as comma-seperated string.
 * @return String Comma-seperated checked symlink directories
 */
function getUncheckedSymlinkDirectories() {
	// Define empty array for symlink directories.
	var uncheckedSymlinkDirectoryArray = Array();
	
	// Iterate through input elements of class 'media-symlink-checkbox'.
	$('input.media-symlink-checkbox').each(function(index, el) {
		// Determine whether element is checked.
		if (!$(this).prop('checked') && !$(this).prop('indeterminate')) {
			// Add value of input attribute 'name' to uncheckedSymlinkDirectoriesArray.
			uncheckedSymlinkDirectoryArray.push($(this).attr('name'));
		};
	});
	
	// Return string representation of uncheckedSymlinkDirectoriesArray (elements separated by commas).
	return uncheckedSymlinkDirectoryArray.join(',');
};

/**
 * initialize the CKEditor
 * @return void
 */
function showCKEditor() {
    $('textarea#page_summary_content_input').ckeditor(
            {
                extraPlugins: 'wordcount',
                wordcount: {
                    showCharCount: false,
                    showWordCount: true,
                    wordLimit: 300
                }
            });
    $('textarea#page_content_input').ckeditor(
            {
                extraPlugins: 'wordcount',
                wordcount: {
                    showCharCount: false,
                    showWordCount: true
                }
            });
};
/**
 * opens a dialog box with the given parameters
 * @param String title the title of the dialog
 * @param String message the message for the dialog
 * @param String title_theme the JQuery Mobile theme letter for the title bar
 * @param String title_theme the JQuery Mobile theme letter for the message box
 * @return void
 */
function openDialog(title, message, title_theme, content_theme) {
    var title = encodeURIComponent(title);
    var message = encodeURIComponent(message);
    var url = '/dialog.php?title=' + title + '&message=' + message + '&title_theme=' + title_theme + '&content_theme=' + content_theme;
    $('#dialog-trigger').attr('href', url).click();
};

function showFormerrorsPopup() {
    $("#formerrors_popup").popup('open');
};
/**
 * get $_GET params from the url
 * @param String name the key of the parameter
 * @param String href the link to get it from
 * @return string
 */
function getParameterByName(name, href) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
};

function bytesToHumanReadableSize(bytes) {
    var humanReadableSize = "";
    var suffix = "";

    if (bytes > 1000000000) {
        bytes /= 1000000000;
        suffix = "GB";
    } else if (bytes > 1000000) {
        bytes /= 1000000;
        suffix = "MB";
    } else if (bytes > 1000) {
        bytes /= 1000;
        suffix = "kB";
    } else {
        suffix = "B";
    };
    var bytesFixed = bytes.toFixed(1);

    humanReadableSize = humanReadableSize.concat(bytesFixed);
    humanReadableSize = humanReadableSize.concat(suffix);

    return humanReadableSize;
};
/**
 * Creates a slider for a progress bar
 * @param String parentElement the JQuery reference to the parent holding the progress bars
 * @param String title the title of the progress bars
 * @param String wrapperId the id of the wrapping element of the progress bar
 * @return void
 */
function createProgressBar(parentElement, title, wrapperId) {
    var wrapper = $('<div />').attr('id', wrapperId);
    $('<label>').appendTo(wrapper).text(title);
    $('<input>').appendTo(wrapper).attr({'name': 'slider', 'class': 'upload-progress', 'data-highlight': 'true', 'min': '0', 'max': '100', 'value': '0', 'type': 'range'}).slider({
        create: function (event, ui) {
            $(this).parent().find('input').hide();
            $(this).parent().find('input').css('margin-left', '-9999px'); // Fix for some FF versions
            $(this).parent().find('.ui-slider-track').css('margin', '0 15px 0 15px');
            $(this).parent().find('.ui-slider-handle').hide();
        }
    });
    wrapper.appendTo(parentElement);
    wrapper.find('input.upload-progress').slider("refresh");
};

// <button class="btnLogin" type="submit" data-theme="a">{t}Update{/t}</button>
function showUpdateButton() {
    $('#update_button').css('display', 'block');
};

function hideFirmwareInputSection() {
    $('#firmware_input_section').css('display', 'none');
};

function showFirmwareInputSection() {
    $('#firmware_input_section').css('display', 'block');
};

/**
 * Appends an error div if the file upload fails
 * @param String fileName the file name if available
 * @param String fileError the error if available
 *
 * @return void
 */
function appendErrorForUpload(fileName, fileError) {
    var shouldAddMessage = true;
    $.each($('div#upload_messages div'), function (index, alert) {
        if ($(alert).attr('data-filename') == fileName) {
            shouldAddMessage = false;
        };
    });
    if (shouldAddMessage === true) {
        if (fileName) {
            errorMsg = "<strong>" + fileName + "</strong>: "
        } else {
            errorMsg = "<strong>Error</strong>: "
        };
        errorMsg = errorMsg + fileError + "<br>";
        var errorDiv = $('<div/>').addClass('alert alert-error').html(errorMsg).attr('data-filename', fileName);
        $('div#upload_messages').append(errorDiv);
        if (!$('div#upload_messages').is(':visible')) {
            $('div#upload_messages').slideDown('slow');
        };
    };
};
/**
 * Appends the success alert div for a successful upload
 *
 * @return void
 */
function appendSuccessfulUpload() {
    var msg = localizedMessages['upload_success'];
    appendSuccessfulUploadWithMsg(msg);
};

function appendSuccessfulUploadWithMsg(msg) {
    var successDiv = $('<div/>').addClass('alert alert-success').html(msg);
    $('div#upload_messages').append(successDiv);
    if (!$('div#upload_messages').is(':visible')) {
        $('div#upload_messages').slideDown('slow');
    };
};

function showPleaseWaitAndClosePopup(popupId){
    if (popupId !== null){
        $(popupId).popup('close');
    };
    showLoadingDisableClicks();
};

function disableClicksEventListner(e){
    e.stopPropagation();
    e.preventDefault();
};

function disableClicks(){
    // disable all clicks source:(http://stackoverflow.com/a/19780264)
    document.addEventListener("click", disableClicksEventListner, true);
};

function showLoadingDisableClicks(){
    $.mobile.loading('show');
    disableClicks();
};
/**
 * extend JQuery's slider for setting it's value
 *
 */
var progressBar = {
    setValue: function (id, value) {
        $(id).val(value);
        $(id).slider("refresh");
    },
    setLabel: function (id, value) {
        $(id).empty();
        $(id).append(value);
    }
}