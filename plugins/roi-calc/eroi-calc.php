<?php



/*



Plugin Name: eROI Calculator



Description: Shows the effects of a donation to ROI Ministries



Version: 2.0



Author: Pete Eigel (YourDesignOnline)



Author URI: http://yourdesignonline.com/



License: All Righst Reserved



*/



//(c) 2012 YourDesignOnline



//error_reporting(0);



function loadCalcJS() {

    //jQuery

wp_register_script('jquery', plugins_url( 'jquery.js' , __FILE__ ));

	wp_enqueue_script( 'jquery' );

	

	//Calculator Logic



	wp_register_script('calculator', plugins_url( 'calc.js' , __FILE__ ));



	wp_enqueue_script('calculator');



	//Style



	wp_register_style( 'calc-style', plugins_url('style.css', __FILE__) );



	wp_enqueue_style( 'calc-style' );



}



add_action('wp_enqueue_scripts', 'loadCalcJS');



function eroi_sc($atts) {



$content =<<<EOD
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="http://www.roiministry.org/wp-content/plugins/eroi-calc/jquery.formatCurrency-1.4.0.js"></script>
<script src="http://www.roiministry.org/wp-content/plugins/eroi-calc/accounting.js"></script>

<script type="text/javascript">
	// Library ready to use:
	accounting.formatMoney(12345678);
</script>
    <SCRIPT LANGUAGE="Javascript">

function setLink() {
if (form.option1.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJW&org=1"; 
	} 
    else 
        if (form.option2.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJT&org=1"; 
	} 
  else 
if (form.option3.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJR&org=1"; 
	} 
  else 
if (form.option4.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJQ&org=1"; 
	} 
else 
if (form.option5.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJS&org=1"; 
	} 
else 
if (form.option6.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzII&org=1"; 
	} 
else 
if (form.option7.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJV&org=1"; 
	} 
else 
if (form.option8.checked){ 
		document.form.action="https://mygiving.secure.force.com/"; 
	} 
else 
if (form.option9.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJP&org=1"; 
	} 
else 
if (form.option10.checked){ 
		document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzJU&org=1"; 
	}
} 
    function twochecks() {
         if ($("#form1 input:checkbox:checked").length > 1) {
  document.form.action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzII&org=";
}
    }
</SCRIPT>
</br></br>
    <div class="roi-calc" id="form1">
     <form name="form" action="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzII&org=" method="post" target="_blank">
    <div class="instructions">
    <div class="leftcc">
    <ol>
    <li>Enter your dollar gift amount</li>
    <li>Select which ministry or ministries you would like to invest in</li>
    <li>Learn more about each ministry by clicking each "Learn More" button</li>
    <li>Click to the right to give directly to the ministries you selected</li>
    </ol>
    </div>
    <div class="buttons rightcc">
    <!--href="https://mygiving.secure.force.com/GXDonateNow?id=a0Ui0000007zzII&org=1"-->
		<p>Click here to give through NFC! Give directly and anonymously</p>
    <input type="submit" value="" class="btncc" id="donate" onclick="twochecks();">		<a class="btncc" href="#openModal11"><img style="max-width:30%;" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/ROI-btn.jpg"/></a>
		<div id="openModal11" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/ROI-btn.jpg" /></h2>
		<p style="font-size:10px;">You will receive a annual giving statement from ROI Ministry. The impersonal dollar amount is not terribly exciting. But if you love making a huge, lasting difference in the lives of many others, then you will get very excited about the lasting impact made today and cumulatively seen in your annual eROI impact statement!</p>
<p style="font-size:10px;">*Based on historical results as provided by ministries. ROI Ministries annually vets all top 10 ministries with the assistance of Calvin Edwards & Company. This information includes, but is not limited to, financial statements, 990s and outcome/impact estimates. All outcomes are based on the measurable dollar results of each ministry, divided by the ministry revenue.
ROI Ministry does not receive any commission for directing you to these organizations.</p>
	</div>
</div>
	</div>
    <br>
	</div>
	<br><br>
    <span class="dollar">$</span><input align="center" type="text" maxlength="7" id="calcGift"  onkeyup="eroiUpdate()" placeholder="0.00" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="amount" /> <br><br>
<p>Enter your gift above and see YOUR impact below!</p> 
<div class="hold">    
<div class="leftcc">
	<ul class="theList">
		<li><ul class="cols"><li><input type="checkbox" id="option1" name="option1" class="cal_input" value="1" onClick="setLink()" /><label for="option1"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/410.jpg" /></li><li><span id="calcValue1">--</span> Villagers with access <br>to safe water for 1 year</li><li><a href="#openModal3" class="LearnMore">Learn More</a><div id="openModal1" class="modalDialog">
	<div> 
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/jesusfilm.jpg" /></h2>
		<p><strong>The Jesus Film Harvest Partners</strong></p>
			<p>A decision for Christ is defined as someone coming forward after an evangelism outreach, such as a JESUS Film showing.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$2.77 per decision for Christ</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option2" name="option2" class="cal_input" value="1" onClick="setLink()" /><label for="option2"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/christtoall.jpg" /></li><li><span id="calcValue2">--</span> Decisions for Christ</li><li><a href="#openModal9" class="LearnMore">Learn More</a><div id="openModal2" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/ilt.jpg" /></h2>
		<p><strong>International Leadership Institute</strong></p>
			<p>A leader trained is defined as a person attending a full ILI conference in which he or she is equipped with the eight core values of effective Christian leaders, and provided with a strategy and curriculum to train other leaders.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$10.69 per leader trained</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option3" name="option3" class="cal_input" value="1" onClick="setLink()" /><label for="option3"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/doulos.jpg" /></li><li><span id="calcValue3">--</span> Decisions for Christ</li><li><a href="#openModal10" class="LearnMore">Learn More</a><div id="openModal3" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/410.jpg" /></h2>
		<p><strong>4:10 Bridge</strong></p>
			<p>Access to safe water is defined as at least 80% of the village’s population has a water access point within one kilometer of their home. The water system is projected to last a minimum of 15 years.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$0.70 per villager per year of access to safe water</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option4" name="option4" class="cal_input" value="1" onClick="setLink()" /><label for="option4"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/logo6.png" /></li><li><span id="calcValue4">--</span> Bible listening groups</li><li><a href="#openModal6" class="LearnMore">Learn More</a><div id="openModal4" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/tmc.jpg" /></h2>
		<p><strong>The Mailbox Club</strong></p>
			<p>A child discipled is defined as completing a series of TMC Bible lessons.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$2.12 per child discipled</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option5" name="option5" class="cal_input" value="1" onClick="setLink()" /><label for="option5"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/gmo.jpg" /></li><li><span id="calcValue5">--</span> Decisions for Christ</li><li><a href="#openModal8" class="LearnMore">Learn More</a><div id="openModal5" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/leg.jpg" /></h2>
		<p><strong>Legacy World Missions</strong></p>
			<p>Providing an orphan with food is defined as an orphan receiving one wholesome meal per day for one month.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$1.52 per orphan with food for a month</p>
	</div>
</div></li></ul>
		</li>
    </ul>
</div>
<div class="rightcc">
	<ul class="theList">
		<li><ul class="cols"><li><input type="checkbox" id="option6" name="option6" class="cal_input" value="1" onClick="setLink()" /><label for="option6"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/ilt.jpg" /></li><li><span id="calcValue6">--</span> Leaders trained</li><li><a href="#openModal2" class="LearnMore">Learn More</a><div id="openModal6" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/logo6.png" /></h2>
		<p><strong>Faith Comes By Hearing</strong></p>
			<p>A Bible listening group is defined as a group of approximately 15 people who have access to five CDs that present the gospel in their native language.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$0.79 per Bible listening group</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option7" name="option7" class="cal_input" value="1" onClick="setLink()" /><label for="option7"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/jesusfilm.jpg" /></li><li><span id="calcValue7">--</span> Decisions For Christ</li><li><a href="#openModal1" class="LearnMore">Learn More</a><div id="openModal7" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/neverthirst.jpg" /></h2>
		<p><strong>Never Thirst</strong></p>
			<p>Access to safe water is defined as having access to a safe water source within 1 kilometer of home.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$0.63 per person per year of access to safe water</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option8" name="option8" class="cal_input" value="1" onClick="setLink()" /><label for="option8"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/leg.jpg" /></li><li><span id="calcValue8">--</span>  Orphans with food<br> for 1 month</li><li><a href="#openModal5" class="LearnMore">Learn More</a><div id="openModal8" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/gmo.jpg" /></h2>
		<p><strong>Global Media Outreach</strong></p>
			<p>A decision for Christ is defined as someone coming forward after an evangelism outreach, such as a JESUS Film showing.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$0.12 per decision for Christ</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option9" name="option9" class="cal_input" value="1" onClick="setLink()" /><label for="option9"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/neverthirst.jpg" /></li><li><span id="calcValue9">--</span> People with access to <br>safe water for 1 year</li><li><a href="#openModal7" class="LearnMore">Learn More</a><div id="openModal9" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/christtoall.jpg" /></h2>
		<p><strong>Christ For All Peoples</strong></p>
			<p>A decision for Christ is defined as a person who saw the JESUS Film in their native language and made a verbal statement that they received Christ</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$3.18 per decision for Christ</p>
	</div>
</div></li></ul>
		</li>
		<li><ul class="cols"><li><input type="checkbox" id="option10" name="option10" class="cal_input" value="1" onClick="setLink()" /><label for="option10"><span></span></label></li><li><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/tmc.jpg" /></li><li><span id="calcValue10">--</span> Children discipled</li><li><a href="#openModal4" class="LearnMore">Learn More</a><div id="openModal10" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2><img class="logocc" src="http://www.roiministry.org/wp-content/plugins/eroi-calc/img/doulos.jpg" /></h2>
		<p><strong>Doulos</strong></p>
			<p>A decision for Christ is defined as someone praying with an indigenous leader to receive Christ.</p>
		<p><strong>Cost Per Outcome:</strong></p>
		<p>$0.60 per decision for Christ</p>
	</div>
</div></li></ul>
		</li>
	</ul>
</div>
    
    </div>
    </form>
    </div>



EOD;



     return $content;



}



add_shortcode('eroi-calc', 'eroi_sc');



?>